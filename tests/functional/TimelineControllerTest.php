<?php

namespace App\Tests\functional;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TimelineControllerTest extends WebTestCase
{
    /**
     * @var Client $client
     */
    private $client;

    protected function setUp()
    {
        $this->client = new Client();
    }

    public function testScreenNameNotProvidedReturns400CodeAndMessage()
    {
        try {
            $this->client->get('http://127.0.0.1:8000/api/timeline');
        } catch (GuzzleException $e) {
            $this->assertEquals(400, $e->getCode());
            $this->assertEquals("Client error: `GET http://127.0.0.1:8000/api/timeline` resulted in a `400 Bad Request` response:\n{\"errors\":\"Screen Name Not Provided\"}\n", $e->getMessage());
        }
    }

    public function testSuccessful200TweetsRequest()
    {
        try {
            $response = $this->client->get('http://127.0.0.1:8000/api/timeline?screen_name=bbc');
            $this->assertEquals(200, $response->getStatusCode());

        } catch (GuzzleException $e) {
            var_dump($e->getMessage());
        }
    }

    public function testResponseCountIsLessThanOrEqualTo20()
    {
        try {
            $response = $this->client->get('http://127.0.0.1:8000/api/timeline?screen_name=bbc');
            $this->assertEquals(200, $response->getStatusCode());

            $tweets = json_decode($response->getBody()->getContents());

            $this->assertTrue(count($tweets) <= 20);

        } catch (GuzzleException $e) {
            var_dump($e->getMessage());
        }
    }
}