<?php

namespace App\Tests\unit\transformer;

use App\Exception\RequiredFieldsException;
use App\Utils\Transformer\TweetTransformer;
use \PHPUnit\Framework\TestCase;
use TypeError;

class TweetTransformerTest extends TestCase
{
    private $expectedKeys = [
        'id',
        'posted_at',
        'text',
        'screen_name',
        'display_picture'
    ];

    /**
     * @var TweetTransformer $transformer
     */
    private $transformer;

    protected function setUp()
    {
        $this->transformer = new TweetTransformer();
    }

    public function testSuccessfulTransformation()
    {
        $goodTweetsList = json_decode(file_get_contents(__DIR__ . '/../../fixtures/resource/goodTweetsList.json'), true);

        try {
            $transformed_tweets = $this->transformer->transformAll($goodTweetsList);

            $this->assertEqualsCanonicalizing($this->getSuccessfulTweetListTransformationProvider(), $transformed_tweets);

            foreach ($transformed_tweets as $tweet) {
                $this->assertEqualsCanonicalizing($this->expectedKeys, array_keys($tweet));
            }

        } catch (RequiredFieldsException $e) {
            var_dump($e->getMessage());
        }
    }

    public function testIncorrectObjectTypeProvided()
    {
        $this->expectException(TypeError::class);

        try {
            //clearly a string is being used for a defined required type of array
            $transformed_tweets = $this->transformer->transformAll('Hello I would like to be transformed please');

            $this->assertEqualsCanonicalizing($this->getSuccessfulTweetListTransformationProvider(), $transformed_tweets);

        } catch (RequiredFieldsException $e) {
            var_dump($e->getMessage());
        }
    }

    private function getSuccessfulTweetListTransformationProvider()
    {
        return [
            [
                'id' => 1284167741168070656,
                'posted_at' => 'Fri Jul 17 16:47:02 +0000 2020',
                'text' => 'What a lovely day it is today.',
                'screen_name' => 'HappyHarry',
                'display_picture' => 'https://pbs.twimg.com/profile_images/random/1234_normal.jpg'
            ],
            [
                'id' => 1284167103025643520,
                'posted_at' => 'Fri Jul 17 16:44:30 +0000 2020',
                'text' => 'Merry Christmas Everyone.',
                'screen_name' => 'HappyHarry',
                'display_picture' => 'https://pbs.twimg.com/profile_images/random/1234_normal.jpg'
            ]
        ];
    }
}