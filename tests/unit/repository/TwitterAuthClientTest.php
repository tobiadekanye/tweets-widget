<?php

namespace App\Tests\unit\repository;

use App\Repository\TwitterAuthClient;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * Class TwitterAuthClientTest
 * @package App\Tests\unit
 */
class TwitterAuthClientTest extends RepoTestCase
{
    public function testSuccessAccessTokenRequest()
    {
        $this->mockClient->method('post')
            ->willReturn(new Response(200, [], file_get_contents(__DIR__ . '/../../fixtures/auth/goodAuthResponse.json')));

        $twitterAuthClient = new TwitterAuthClient("some_auth_key", "some_auth_secret", $this->mockClient);

        $access_token = $twitterAuthClient->requestToken();

        $this->assertEquals('AAAAAAAAAAAAAAAAAAAAAHq7uQAAAAAAp32tVZEi6NxA%2FBAGocxb8EKxbys%3DtwQNnNOKU1LtuDaaGq3dZIEQDj9GQkGvE3mIKKYNqhPcD6ZcDr', $access_token);
    }

    public function testUnauthorizedAccessTokenRequest()
    {
        $this->mockClient->method('post')
            ->willReturn(new Response(403, [], file_get_contents(__DIR__ . '/../../fixtures/auth/forbiddenAuthResponse.json')));

        $twitterAuthClient = new TwitterAuthClient("some_auth_key", "some_auth_secret", $this->mockClient);

        $access_token = $twitterAuthClient->requestToken();

        $this->assertNull($access_token);
    }

    public function testBadAccessTokenRequest()
    {
        $this->mockClient->method('post')
            ->willReturn(new Response(401, [], file_get_contents(__DIR__ . '/../../fixtures/auth/badRequestAuthResponse.json')));

        $twitterAuthClient = new TwitterAuthClient("some_auth_key", "some_auth_secret", $this->mockClient);

        $access_token = $twitterAuthClient->requestToken();

        $this->assertNull($access_token);
    }
}