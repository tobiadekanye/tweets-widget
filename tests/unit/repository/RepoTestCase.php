<?php


namespace App\Tests\unit\repository;

use GuzzleHttp\Client;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class RepoTestCase extends TestCase
{
    /**
     * @var MockObject $mockClient
     */
    protected $mockClient;

    protected function setUp()
    {
        $this->mockClient = $this->createMock(Client::class);
    }

}