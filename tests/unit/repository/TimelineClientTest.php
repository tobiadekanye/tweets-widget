<?php

namespace App\Tests\unit\repository;

use App\Repository\TweetClient;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response;

class TimelineClientTest extends RepoTestCase
{
    public function testSuccessRequestTweets()
    {
        $this->mockClient->method('get')
            ->willReturn(new Response(200, [], file_get_contents(__DIR__ . '/../../fixtures/resource/fullGoodTweetsResponse.json')));

        $timelineClient = new TweetClient($this->mockClient);

        try {
            $response = $timelineClient->requestTweets('bbc', 'abc123');

            $this->assertTrue(count($response) === 20);

        } catch (GuzzleException $e) {
            var_dump($e->getMessage());
        }
    }

    public function testNoScreenNameRequestTweets()
    {
        $this->mockClient->method('get')
            ->willReturn(new Response(401, [], file_get_contents(__DIR__ . '/../../fixtures/resource/unauthorizedTimelineResponse.json')));

        $timelineClient = new TweetClient($this->mockClient);

        try {
            $response = $timelineClient->requestTweets('', 'abc123');

            $this->assertArrayHasKey('request', $response);
            $this->assertArrayHasKey('error', $response);
            $this->assertStringContainsString("Not authorized.", $response['error']);

        } catch (GuzzleException $e) {
            var_dump($e->getCode(), $e->getMessage());
        }
    }
}