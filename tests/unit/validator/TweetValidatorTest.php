<?php

namespace App\Tests\unit\validator;

use App\Exception\RequiredFieldsException;
use App\Utils\Validator\TweetValidator;
use PHPUnit\Framework\TestCase;

class TweetValidatorTest extends TestCase
{
    private $validator;

    protected function setUp()
    {
        $this->validator = new TweetValidator();
    }

    /**
     * Happy path test
     */
    public function testPassValidation()
    {
        // Complete JSON of Tweet, no missing fields and correctly composed as expected
        $goodTweet = json_decode(file_get_contents(__DIR__ . '/../../fixtures/resource/goodTweet.json'), true);

        try {
            $this->assertNull($this->validator->validate($goodTweet));
        } catch (RequiredFieldsException $e) {
            var_dump($e->getMessage());
        }
    }

    /**
     * Missing required fields test
     * @throws RequiredFieldsException
     */
    public function testFailedValidation()
    {
        $this->expectException(RequiredFieldsException::class);

        //This JSON has missing fields: text, screen_name so is expected to fail validation
        $badTweet = json_decode(file_get_contents(__DIR__ . '/../../fixtures/resource/missingFieldsTweet.json'), true);

        $this->validator->validate($badTweet);
    }

    /**
     * Testing expected exception message
     */
    public function testFailedValidationErrorMessage()
    {
        //This JSON has missing fields: text, screen_name so is expected to fail validation
        $badTweet = json_decode(file_get_contents(__DIR__ . '/../../fixtures/resource/missingFieldsTweet.json'), true);

        try {
            $this->validator->validate($badTweet);
        }catch (RequiredFieldsException $e) {
            $this->assertEquals('Required Fields Not Found: text, screen_name', $e->getMessage());
        }
    }
}