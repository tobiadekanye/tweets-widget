local-start:
	@echo "Starting symfony & react front end applications"
	composer install
	yarn install
	symfony server:start -d && yarn watch

run-tests: run-unit-tests run-functional-tests

run-unit-tests:
	@echo "Running unit tests"
	php ./vendor/bin/simple-phpunit ./tests/unit

run-functional-tests:
	@echo "Running functional tests"
	php ./vendor/bin/simple-phpunit ./tests/functional

stop:
	@echo "Stopping Server"
	symfony server:stop

### Docker related make commands ###

# Entry point command as seen in Dockerfile
container-app-start:
	@echo "Starting symfony & react front end applications"
	yarn watch

docker-start: docker-build
	@echo "Building & Running Twitter Widget Container"
	docker-compose up -d

docker-build:
	@echo "Building Twitter Widget Container"
	docker-compose build

docker-stop:
	@echo "Shutting Down Twitter Widget Container"
	docker-compose down

docker-run-tests:
	@echo "Running within docker container"
	docker exec -it tweets-widget_app_1 make run-tests

docker-run-unit-tests:
	@echo "Running within docker container"
	docker exec -it tweets-widget_app_1 make run-unit-tests

docker-run-functional-tests:
	@echo "Running within docker container"
	docker exec -it tweets-widget_app_1 make run-functional-tests


## Artifact ##

create-artifact: docker-build
	@echo "Creating image of tweets-widget_app"
	sh ../scripts/create-image.sh