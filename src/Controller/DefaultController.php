<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * Render the index.html.twig template
     * which contains the react app root binder for it to be loaded onto
     *
     * @Route("/")
     */
    public function index(): Response
    {
        return $this->render('default/index.html.twig');
    }

    /**
     * @Route("/home")
     */
    public function homepage(): Response
    {
        return $this->render('home/index.html.twig');
    }
}