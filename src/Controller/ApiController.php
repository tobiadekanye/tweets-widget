<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class ApiController extends AbstractController
{
    /**
     * Default 200
     * @var int $statusCode
     */
    protected $statusCode = 200;

    /**
     * Gets the value of statusCode.
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * Set statusCode
     * @param int $statusCode
     * @return $this
     */
    protected function setStatusCode($statusCode): ApiController
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * Return JSON response containing status code
     * @param array $data
     * @return JsonResponse
     */
    public function response($data): JsonResponse
    {
        return new JsonResponse($data, $this->getStatusCode());
    }

    /**
     * Set errors to be used in data response.
     * Returns a JSON response
     *
     * @param string $errors
     * @return JsonResponse
     */
    public function errorResponse($errors): JsonResponse
    {
        $data = [
            'errors' => $errors,
        ];

        return new JsonResponse($data, $this->getStatusCode());
    }
}