<?php

namespace App\Controller;

use App\Model\User;
use App\Utils\Assembler\UserModeller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

final class ModellerController extends ApiController
{
    /**
     * @var UserModeller
     */
    private $modeller;

    /**
     * ModellerController constructor.
     * @param UserModeller $modeller
     */
    public function __construct(UserModeller $modeller)
    {
        $this->modeller = $modeller;
    }

    public function all(Request $request): JsonResponse
    {
        // Do some repository stuff to get the data

        // Use the modeller to get the links associated to that model.

        $theCompletedModel = [];

        $this->modeller::toModel(new User(1, 'Tobi', 'tobi@email.com'));

        return $this->setStatusCode(200)->response($theCompletedModel);

    }
}