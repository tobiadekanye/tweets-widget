<?php

namespace App\Controller;

use App\Exception\RequiredFieldsException;
use App\Repository\TwitterAuthClient;
use App\Service\TweetService;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * TimelineController Controller
 *
 * From the twitter api documentation https://developer.twitter.com/en/docs/tweets/timelines/api-reference/get-statuses-user_timeline
 * This endpoint best represents what we want. Providing a "screen_name" e.g. bbc
 * and will provide the accounts latest tweets. This is the endpoint I will develop against
 */
final class TimelineController extends ApiController {

    /**
     * @var TweetService $tweetService
     */
    private $tweetService;

    /**
     * @var LoggerInterface $logger
     */
    private $logger;

    /**
     * @var TwitterAuthClient $twitterAuthClient
     */
    private $twitterAuthClient;

    /**
     * TimelineController constructor.
     *
     * @param TweetService $tweetService
     * @param TwitterAuthClient $twitterAuthClient
     * @param LoggerInterface $logger
     */
    public function __construct(
        TweetService $tweetService,
        TwitterAuthClient $twitterAuthClient,
        LoggerInterface $logger
    ) {
        $this->tweetService = $tweetService;
        $this->twitterAuthClient = $twitterAuthClient;
        $this->logger = $logger;
    }

    /**
     * Tweet timeline for a given twitter account identified by screen_name
     * @Route("/api/timeline", name="get_timeline", methods={"GET"})
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getTimeline(Request $request): JsonResponse {

        // get the screenName request query parameter.
        $screenName = $request->query->get('screen_name');

        if(!$screenName) {
            return $this->setStatusCode(400)->errorResponse("Screen Name Not Provided");
        }

        //log the screenName
        $this->logger->info('Provided Screen Name: @' . $screenName);

        //request access_token
        $this->logger->info('Fetching Access Token');
        $accessToken = $this->twitterAuthClient->requestToken();

        if($accessToken === null) {
            //log failure reason
            $this->logger->error('Failed access token request');

            //respond with failed internal error status code and error reason;
            return $this->setStatusCode(500)->errorResponse('Unable to obtain access token');
        }

        $this->logger->info('Successfully Obtained Access Token');

        try {
            $tweets = $this->tweetService->getTweets($screenName, $accessToken);

            return $this->response($tweets);
        } catch (GuzzleException $e) {
            //log exception error
            $this->logger->error('Guzzle Failure Response - Status Code: ' . $e->getCode() . ' - Message: ' . $e->getMessage());

            return $this->setStatusCode($e->getCode())->errorResponse($e->getMessage());
        } catch (RequiredFieldsException $e) {
            //log exception error
            $this->logger->error($e->getMessage());

            return $this->setStatusCode($e->getCode())->errorResponse($e->getMessage());
        }
    }
}