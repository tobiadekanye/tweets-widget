<?php

namespace App\Utils\Assembler;

use App\Model\Entity;
use App\Model\EntityModel;

class UserModeller
{
    public function __construct()
    {

    }

    public static function toModel(Entity $entity)
    {
        $entityModel = new EntityModel($entity);
        $entityModel->addLink('self', 'UserController');
    }
}