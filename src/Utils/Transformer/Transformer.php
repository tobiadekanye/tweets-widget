<?php

namespace App\Utils\Transformer;

use App\Exception\RequiredFieldsException;

/**
 * Interface TransformerInterface
 * @package App\Utils
 */
interface Transformer
{
    /**
     * Singular intended object
     * @param array $object
     * @return mixed
     * @throws RequiredFieldsException
     */
    public function transform(array $object);

    /**
     * List of intended objects
     * @param array $objects
     * @return mixed
     * @throws RequiredFieldsException
     */
    public function transformAll(array $objects);
}