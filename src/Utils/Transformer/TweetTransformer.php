<?php

namespace App\Utils\Transformer;

use App\Exception\RequiredFieldsException;
use App\Utils\Validator\TweetValidator;

/**
 * Class TweetTransformer
 * @package App\Utils
 */
class TweetTransformer implements Transformer
{
    /**
     * @param array $tweet
     * @return array
     */
    public function transform(array $tweet)
    {
        return [
            'id' => $tweet['id'],
            'posted_at' => $tweet['created_at'],
            'text' => $tweet['text'],
            'screen_name' => $tweet['user']['screen_name'],
            'display_picture' => $tweet['user']['profile_image_url_https']
        ];
    }

    /**
     * @param array $tweets
     * @return array
     */
    public function transformAll(array $tweets)
    {
        $transformed_tweets = [];

        foreach ($tweets as $tweet) {
            $transformed_tweets[] = $this->transform($tweet);
        }

        return $transformed_tweets;
    }
}