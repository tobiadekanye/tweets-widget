<?php

namespace App\Utils\Validator;

/**
 * Interface Validator
 * @package App\Utils\Validator
 */
interface Validator
{
    /**
     * @param $object
     * @return mixed
     */
    public function validate($object);
}