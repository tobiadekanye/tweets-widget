<?php

namespace App\Utils\Validator;

/**
 * Class AbstractValidator
 * Defining a base validator class which consists of a property that its subclasses will be performing
 * validation against.
 *
 * @package App\Utils
 */
abstract class AbstractValidator
{
    /**
     * @var array $required
     */
    protected $required = [];
}