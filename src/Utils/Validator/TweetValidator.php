<?php

namespace App\Utils\Validator;

use App\Exception\RequiredFieldsException;

/**
 * Class RequiredFieldValidator
 * @package App\Utils
 *
 * Could look into the Symfony/Validator package
 */
class TweetValidator extends AbstractValidator implements Validator
{
    /**
     * @var string[] $required
     */
    protected $required = [
        'id',
        'created_at',
        'text',
        'screen_name',
        'profile_image_url_https'
    ];

    /**
     * Singular object validation against required fields
     *
     * @param $object
     * @throws RequiredFieldsException
     */
    public function validate($object): void
    {
        $objectKeys = array_merge(array_keys($object), array_keys($object["user"]));
        foreach ($object as $key => $value) {
            $this->hasRequiredFields($objectKeys);
        }
    }

    /**
     * Take in list of objects for validation
     * @param array $objects
     * @throws RequiredFieldsException
     */
    public function validateList(array $objects): void
    {
        foreach ($objects as $object) {
            $this->validate($object);
        }
    }

    /**
     * Using the passed in required fields, check the object has all of the required keys.
     * @param array $keys
     * @throws RequiredFieldsException
     */
    private function hasRequiredFields(array $keys)
    {
        $missingFields = [];

        foreach($this->required as $key => $value) {
            if(!in_array($value, $keys)) {
                $missingFields[] = $value;
            }
        }

        if(count($missingFields) > 0) {
            throw new RequiredFieldsException('Required Fields Not Found: ' . implode(", ", $missingFields));
        }
    }
}