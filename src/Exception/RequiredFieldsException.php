<?php

namespace App\Exception;

use Exception;

class RequiredFieldsException extends Exception
{
    /**
     * RequiredFieldsException constructor.
     * @param string $message
     */
    public function __construct(string $message)
    {
        parent::__construct($message);
        $this->code = 500;
    }
}