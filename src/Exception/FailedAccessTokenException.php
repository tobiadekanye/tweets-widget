<?php

namespace App\Exception;

use Exception;

class FailedAccessTokenException extends Exception
{
    /**
     * FailedAccessTokenException constructor.
     */
    public function __construct()
    {
        parent::__construct("Unable to get Access Token");
        $this->code = 401;
    }
}