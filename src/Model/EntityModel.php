<?php

namespace App\Model;

class EntityModel
{
    private $rels;

    /**
     * EntityModel constructor.
     * @param Entity $entity
     */
    public function __construct(Entity $entity)
    {
        $this->id = $entity->getId();
        $this->rels = [];
    }

    public function addLink($rel, string $link)
    {
        $this->rels[$rel] = $link;
    }
}