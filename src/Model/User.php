<?php


namespace App\Model;


class User implements Entity
{
    private $id;

    private $name;

    private $email;

    /**
     * User constructor.
     * @param $id
     * @param $name
     * @param $email
     */
    public function __construct($id, $name, $email)
    {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
    }

    public function getId()
    {
        return $this->id;
    }
}