<?php

namespace App\Model;

interface Entity
{
    public function getId();
}