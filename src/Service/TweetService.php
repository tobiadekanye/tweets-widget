<?php

namespace App\Service;

use App\Exception\RequiredFieldsException;
use App\Repository\TweetClient;
use App\Utils\Transformer\Transformer;
use App\Utils\Validator\TweetValidator;
use GuzzleHttp\Exception\GuzzleException;

class TweetService
{
    /**
     * @var TweetClient
     */
    private $tweetClient;

    /**
     * @var Transformer
     */
    private $transformer;

    /**
     * @var TweetValidator
     */
    private $validator;

    /**
     * TweetService constructor.
     * @param TweetClient $tweetClient
     * @param Transformer $tweetTransformer
     * @param TweetValidator $tweetValidator
     */
    public function __construct(
        TweetClient $tweetClient,
        Transformer $tweetTransformer,
        TweetValidator $tweetValidator
    ) {
        $this->tweetClient = $tweetClient;
        $this->transformer = $tweetTransformer;
        $this->validator = $tweetValidator;
    }

    /**
     * Service level handler of requesting tweets from twitter, validating them and transforming to widget
     * desired format. Additional business logic can be added here if needed.
     * @param string $screenName
     * @param string $accessToken
     * @return array
     * @throws GuzzleException
     * @throws RequiredFieldsException
     */
    public function getTweets(string $screenName, string $accessToken):array {

        // Request Tweets
        $tweets = $this->tweetClient->requestTweets($screenName, $accessToken);

        //Validate received tweets to contain the required fields
        $this->validator->validateList($tweets);

        //Transform the tweets and return the output/
        return array_map(function($tweet) {
            return $this->transformer->transform($tweet);
        }, $tweets);
    }
}