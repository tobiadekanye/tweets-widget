<?php

namespace App\Repository;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class TweetClientBase
 *
 * If decided to not autowire the GuzzleClient at this level, we could create a base BaseRestClient class
 * that configures the GuzzleClient client to be used by its subclasses, who will provide the
 * necessary configuration for their use case i.e. constructing using a baseurl provided by the subclass
 *
 * @package App\Repository
 */
class TweetClient
{
    /**
     * @var Client
     */
    protected $client;

    public function __construct($client)
    {
        $this->client = $client;
    }

    /**
     * Request tweets for given screen_name with token used in Bearer Authorization header
     * i've decided to go with a count of 20
     *
     * @param string $screen_name
     * @param string $token
     * @return array|null
     * @throws GuzzleException
     */
    public function requestTweets(string $screen_name, string $token): array
    {
        $response = $this->client->get('https://api.twitter.com/1.1/statuses/user_timeline.json', [
            'query' => [
                'screen_name' => $screen_name, 'count' => 20
            ],
            'headers' => [
                'Authorization' => "Bearer ${token}"
            ]
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }
}