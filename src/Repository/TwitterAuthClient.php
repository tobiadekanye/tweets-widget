<?php

namespace App\Repository;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Possibility of abstracting even further with a base Repository class take takes in a base url for
 * the GuzzleClient to be set up with. Then subclasses can state urls after the domain.
 *
 * Class TwitterAuthClientBase
 * @package App\Repository
 */
class TwitterAuthClient
{

    /**
     * @var Client $client
     */
    private $client;

    /**
     * @var string $auth_key
     */
    private $auth_key;

    /**
     * @var string $auth_secret
     */
    private $auth_secret;

    /**
     * TwitterAuthClientBase constructor.
     * These parameters are obtained through the services autowired arguments.
     * Located in the .env file and obtained into the parameters & services section of the services.yml
     *
     * @param string $auth_key
     * @param string $auth_secret
     * @param $client
     */
    public function __construct($auth_key, $auth_secret, $client)
    {
        $this->auth_key = $auth_key;
        $this->auth_secret = $auth_secret;
        $this->client = $client;
    }

    /**
     * Request Access Token from the Twitter API auth endpoint using the auth properties
     *
     * @return string|null
     */
    public function
    requestToken(): ?string
    {
        try {
            $response = $this->client->post('https://api.twitter.com/oauth2/token',
                [
                    'auth' => [
                        $this->auth_key,
                        $this->auth_secret,
                    ],
                    'form_params' => [
                        'grant_type'=> 'client_credentials'
                    ]
                ]
            );

            return json_decode($response->getBody()->getContents(), true)['access_token'] ?? null;

        } catch (GuzzleException $e) {

            return null;
        }
    }
}