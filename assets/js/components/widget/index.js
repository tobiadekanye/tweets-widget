import React, { useState, useEffect } from 'react';
import { Container, Snackbar } from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import Header from '../header';
import TweetPanel from '../tweet_panel';
import * as localStorage from 'local-storage';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
    container: {
        height: '100%', 
        display: 'flex', 
        alignItems: 'center', 
        justifyContent: 'center'
    },  
    widget: {
        border: '1px solid #dddcdc', 
        borderRadius: '5px',
        backgroundColor: '#55aced'
    },
}));

const TweetsWidget = () => {
    const classes = useStyles();

    const [tweetsState, setTweets] = useState({tweets: [], loading: false});

    const [twitterHandle, setTwitterHandle] = useState(localStorage.get('last_successful') ? localStorage.get('last_successful') : '');    
    
    const [showError, setShowError] = useState(false);

    const getTweets = async (screen_name) => {

        setTweets({...tweetsState, loading: true});
    
        try {
            let response = await fetch(`/api/timeline?screen_name=${screen_name}`);
            
            if(!response.ok) {
                setShowError(true);
                setTweets({...tweetsState, loading: false});
                return;
            }

            setShowError(false);

            let newTweets = await response.json();

            setTweets({tweets: newTweets, loading: false});
            
            localStorage.set('last_successful', screen_name);

        } catch(err) {
            setShowError(true);
            setTweets({...tweetsState, loading: false});
        }
    }

    useEffect(() => {

        let last_successful = localStorage.get('last_successful') ? localStorage.get('last_successful') : 'bbc';

        getTweets(twitterHandle !== '' ? twitterHandle : last_successful);

        const interval = setInterval(() => {
            getTweets(twitterHandle !== '' ? twitterHandle : last_successful);
        }, 20000);

        return () => {
          clearInterval(interval);
        };

    }, [twitterHandle]);

    return (
        <div className={classes.container}>
            <Container maxWidth="xs">
                <div className={classes.widget}>
                    <Header handleChange={(event) => setTwitterHandle(event.target.value)} screenName={twitterHandle}/>
                    <TweetPanel tweets={tweetsState.tweets} loading={tweetsState.loading}/>
                </div>
                <Snackbar
                    anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
                    open={showError}
                    autoHideDuration={1000}
                    onClose={() => setShowError(false)}
                >
                    <Alert severity="error">
                        {`Unable to fetch inputed users tweets.`}
                    </Alert>
                </Snackbar>
            </Container>
        </div>
    )
}

export default TweetsWidget;