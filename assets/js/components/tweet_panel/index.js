import React from 'react';
import Loader from '../loader';
import TweetList from '../tweet_list';
import { Grid } from '@material-ui/core';

const TweetPanel = ({tweets, loading}) => {

    return (
        <Grid item xs={12}>
            <div style={{maxHeight: '640px', minHeight: '640px', position: 'relative', overflowY: 'scroll', backgroundColor: 'white'}}>
                {!loading ? <TweetList data={tweets}/> :<Loader/>}
            </div>
        </Grid>
    );
}

export default TweetPanel;