import React from 'react';
import { Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Row } from 'antd'
import twitter_logo from '../../../twitter-logo.png';
import TitledInput from '../titled_input';

const useStyles = makeStyles(() => ({
    header: {
        width: '100%'
    },
    inputPanel: {
        padding: 5,
        margin: 10,
        backgroundColor: 'white',
        borderRadius: 5,
        textAlign: 'left'
    }
}));

const Header = ({handleChange, screenName}) => {
    const classes = useStyles();

    return(
        <Grid 
            item xs={12} 
            className={classes.header}
        >
            <Row align="middle" className={classes.inputPanel}>
                <TitledInput title="@'s Latest Tweets" size="large" value={screenName} onChange={handleChange} style={{border: "none", fontWeight: 'bold'}}/>

                <div style={{marginLeft: 100}}>
                    <img src={twitter_logo} style={{width: 'auto', height: 40}} alt="twitter_logo"/>
                </div>
            </Row>
        </Grid>
    );
}

export default Header;