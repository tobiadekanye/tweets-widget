import React from 'react';
import { List } from '@material-ui/core';
import Tweet from '../tweet';

const TweetList = ({data}) => {
    return(
        <List>
            {
                data.map((tweet) => {
                    return(
                        <div key={tweet.id}>
                            <Tweet displayPicture={tweet.display_picture} text={tweet.text} postedAt={tweet.posted_at}/>
                        </div>
                    )
                })
            }
        </List>
    );
}

export default TweetList;