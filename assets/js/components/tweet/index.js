import React, { useState } from 'react';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import { 
    Divider, 
    ListItem, 
    ListItemText, 
    ListItemSecondaryAction, 
    ListItemAvatar, 
    Avatar, 
    Typography 
} from '@material-ui/core';

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
    tweet: {
        '&:hover': { 
            backgroundColor: '#e6f6ff'
        }
    },
    delete: {
        '&:hover': {
            cursor: 'pointer'
        },
        '&:active': {
            color: 'red'
        }
    },
    hide: {
        display: 'none'
    }
}));

const Tweet = ({displayPicture, text, postedAt}) => {

    const classes = useStyles();

    const [show, setShow] = useState(true);

    return (
        <div className={`${show ? '' : classes.hide}`}>
            <ListItem alignItems="flex-start" className={classes.tweet} >
                <ListItemAvatar>
                    <Avatar src={displayPicture} />
                </ListItemAvatar>
                <ListItemText
                    primary={text}
                    secondary={
                        <React.Fragment>
                            <Typography
                                component="span"
                                variant="body2"
                                style={{display: 'inline'}}
                                color="textPrimary"
                            >
                                {postedAt.replace(/ \+.*/g, '')}
                            </Typography>
                        </React.Fragment>
                    }
                />
                <ListItemSecondaryAction className={classes.delete}>
                    <DeleteForeverIcon onClick={() => setShow(false)} />
                </ListItemSecondaryAction>
            </ListItem>
            <Divider/>
        </div>
    );
}

export default Tweet;