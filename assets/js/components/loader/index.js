import React from 'react';
import { CircularProgress, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  centre: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)'
  },
}));

const Loader = () => {
    const classes = useStyles();

    return(
        <div className={classes.centre}>
            <div>
              <Typography variant="subtitle1" gutterBottom>
                Searching Tweets
              </Typography>
              <div style={{textAlign: 'center'}}>
                <CircularProgress/>
              </div>
            </div>
        </div>
    );
}

export default Loader;