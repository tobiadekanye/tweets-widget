import React from 'react';
import { Input, Typography } from 'antd'

const { Text } = Typography;

const TitledInput = ({title, size, value,  ...rest}) => {
    return(
        <div>
            <span style={{marginLeft: 10}}>
                <Text style={{fontSize: 15}} type="secondary">{title}</Text>
            </span>
            <div style={{paddingTop: 10, marginTop: -10}}>
                <Input size={size} {...rest}/>
            </div>
        </div>
    );
}

export default TitledInput;