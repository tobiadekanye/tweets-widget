#bin/bash!
echo "Creating Tweets Widget Artifact"

docker save tweets-widget_app > ./`date +%m-%d-%Y`-tweets-widget_app.tar

echo "Pruning system images"

docker system prune -f