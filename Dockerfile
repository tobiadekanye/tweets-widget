FROM php:7.4-apache-buster

RUN apt-get update

RUN apt-get install -y git unzip zip curl

COPY ./server-config/vhost.conf /etc/apache2/sites-available/000-default.conf

RUN a2enmod rewrite headers 

RUN cp /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini
RUN pecl install xdebug
RUN echo "zend_extension=/usr/local/lib/php/extensions/no-debug-non-zts-20190902/xdebug.so" >> /usr/local/etc/php/php.ini
ENV XDEBUG=true


RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install Node & Yarn
RUN curl -sL https://deb.nodesource.com/setup_14.x | bash -
RUN apt-get install -y nodejs && node -v
RUN npm install -g yarn && yarn -v

COPY . /var/www/html

RUN composer install

RUN yarn install