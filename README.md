# Twitter Widget

A simple application using a Backend API & Frontend application that fetches tweets for a given twitter name.

## Technologies

Backend: `PHP` Symfony

Frontend: `Javascript` React JS

Binded Together in one codebase using `Webpack Encore` & `Twig Templates`

And `Docker`

## Repository

PHP code can be found via the `./appsrc` directory 

Javascript code can be found via the `./app/assets/js` directory

## Starting The Application

Provided are Make commands to help simplify running the bootup instructions of the app via local machine or docker.

REMEMBER TO `cd app` before running these make commands.

### Local

`cd app && make local-start` -  From the root of the repo go into the app directory and execute the make local-start command

Once booted up, you can access the application UI via the browser url `http://127.0.0.1:8000/` - (8000 is the default port, if already
occupied a different port will used e.g. 8001)

### Docker

The Application has also been dockerised.

The make commands within `app/Makefile` have been updated with additional commands for running the app within a docker container.

`make docker-start` - Will build and up the container which can then be accessed via `http://localhost:80` as indiciated via the `ports` section of the `docker-compose.yml`

`make docker-stop` - Will stop and bring down the container

## Widget Usage 
Using the input field, enter a 'twitter handle', known as the `screen_name`. For Example "bbc"

If the twitter handle exists you will be provided the 20 latest tweets from that account.

The tweets will be re-fetched at a 20-second interval.

Each time a successful search has been performed, the twitter hanle will be stored as a local-storage variable `last_successful`.

The `bin` symbol along each tweet entry allows you to remove that tweet from the tweet list.

## Errors
If unable to identify the provided twitter handle, an error snackbar will be shown in the bottom centre indicating the inability to fetch the entered accounts tweets.

## Tests

A Makefile has been provided which contains short-hand commands to executing tests within the `/tests` folder.

`make run-tests` - Functional & Unit Tests

`make run-functional-tests` - Functional Tests

`make run-unit-tests` - Unit Tests 

On running tests within the docker container, please give a bit of time for the instance to stabilise before running.
More time to look into optimising the bootup of the container is needed.

`make docker-run-tests` - Will execute functional and unit tests from within the container. You will also find the other test related
docker commands for running tests. `docker-functional-tests` & `docker-unit-tests`

## Fixtures
Fixtures produced by calling the twitter api via Postman.

These files are stored under the `/tests/fixtures` path.

The Postman collection used for development is provided in `Twitter_API_Auth_Timeline_Collection.postman_collection.json`


# Considerations

1. Separating the Frontend application from the Backend Codebase. This will remove and reduce project dependencies and allow for more bespoke handling of deployment/configuration etc.

- React static application hosted via S3 static site hosting and cloudfront

- Dockerised Symfony API Backend running on an EC2 instance.

2. Moving validation/transformation/fetching into a service. The controller would simple reference the service method that contains these actions and this will allow for additional business logic to be added if desired

3. TODO - Frontend testing of components and E2E. Currently difficult as a test environment will be required to be built up for tests to be ran on. Potentially doable through docker, spin up container, run tests, exit, down. TBD

- Cypress

- testing-library/react
