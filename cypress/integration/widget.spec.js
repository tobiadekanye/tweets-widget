/// <reference types="cypress" />

describe('Widget Test', () => {
    beforeEach(() => {
        cy.visit('/');
    });

    it('It can see the tweets widget!', () => {
        cy.visit('/');
        cy.contains('@\'s Latest Tweets');
    });
})