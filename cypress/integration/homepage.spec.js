/// <reference types="cypress" />

describe('Homepage Behavioural Test', () => {
    beforeEach(() => {
        cy.visit('/');
    });

    it('It can find the content "Welcome to the Tweets Widget" !', () => {
        cy.visit('/home');
        
        cy.contains("Welcome to the Tweets Widget");
    });
})